const axios = require("axios");

// CREA UNA TAREA
async function crearTarea(titulo) {
  try {
    // Hace la petición al Rest API
    let respuesta = await axios.post("http://localhost:3000/tareas", {
      titulo: titulo,
    });
    // Retorna el resultado
    return respuesta.data;
  } catch (error) {
    console.log(error);
  }
}

// OBTIENE TODAS LAS TAREAS
async function obtenerTodaslasTareas() {
  try {
    // Hace la petición al Rest API
    let tareas = await axios.get("http://localhost:3000/tareas");
    // Muestra las tareas
    return tareas.data;
  } catch (error) {
    console.log(error);
  }
}

// OBTIENE UNA LAS TAREA
async function obtenerTarea(id) {
  try {
    // Hace la petición al Rest API
    let tarea = await axios.get(`http://localhost:3000/tareas/${id}`);
    // Muestra la tarea
    return tarea.data;
  } catch (error) {
    console.log(error);
  }
}

// MODIFICAR UNA TAREA
async function modificarTarea(id, titulo) {
  try {
    // Hace la petición al Rest API
    let respuesta = await axios.patch(`http://localhost:3000/tareas/${id}`, {
      titulo: titulo,
    });
    // Muestra el resultado
    return respuesta.data;
  } catch (error) {
    console.log(error);
  }
}

// ELIMINAR UNA TAREA
async function eliminarTarea(id) {
  try {
    // Hace la petición al Rest API
    let respuesta = await axios.delete(`http://localhost:3000/tareas/${id}`);
    // Muestra el resultado
    return respuesta.data;
  } catch (error) {
    console.log(error);
  }
}

async function ejecucion() {
  // Crea una tarea
  // let respuesta = await crearTarea("swimming");
  // console.log(respuesta);

  // Obtiene todas las tareas
  // let tareas = await obtenerTodaslasTareas();
  // console.log(tareas);

  // Obtiene una tarea
  let tarea = await obtenerTarea("5f9254c954e8c2e5f1a5eb83");
  console.log(tarea);

  // Modifica la tarea
  // let tarea = await modificarTarea("5f9254c954e8c2e5f1a5eb83", "climb");
  // console.log(tarea);

  // Elimina la tarea
  // let respuesta = await eliminarTarea("5f9254c954e8c2e5f1a5eb83");
  // console.log(respuesta);
}

// Ejecuta la función
ejecucion();
